// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function getSpecificCars( inventory ){
    const Audi_BMW_Arr = [];

    for( let index=0; index<inventory.length; index++ ){
        if(inventory[index].car_make == 'BMW' || inventory[index].car_make == 'Audi'){
            Audi_BMW_Arr.push(inventory[index]);
        }
    }
    if( Audi_BMW_Arr.length == 0){
        return null;
    }
    return Audi_BMW_Arr;
}

module.exports = {getSpecificCars}