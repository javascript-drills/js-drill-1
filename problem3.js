// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCars( inventory ){
    const carModel_Arr = [];
    for( let index=0; index<inventory.length; index++ ){
        carModel_Arr.push(inventory[index].car_model);
    }

    //Sorting the array now
    for( let index=0; index<inventory.length; index++ ){
        for( let j=0; j<inventory.length-index-1; j++){
            if(carModel_Arr[j] > carModel_Arr[j+1]){
                let temp = carModel_Arr[j];
                carModel_Arr[j] = carModel_Arr[j+1];
                carModel_Arr[j+1] = temp;
            }
        }
    }
    return carModel_Arr;
}

module.exports = {sortCars};