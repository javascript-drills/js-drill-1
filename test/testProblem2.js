const { inventory } = require("../dataSet");
const { lastCar } = require("../problem2");

const carData = lastCar(inventory);
if(lastCar){
    console.log(`Last car is a ${carData.car_make} ${carData.car_model}`);
}
else{
    console.log("No data available");
}