const { inventory } = require("../dataSet");
const { sortCars } = require("../problem3");

const carModel_data = sortCars(inventory);
if( carModel_data ){
    console.log(carModel_data);
}
else{
    console.log("No data available");
}