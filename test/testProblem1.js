const { inventory } = require("../dataSet");
const { carInfo } = require("../problem1");

const carData = carInfo( inventory, 33 );
 if( carData === null ){
    console.log("No data available");
 }
 else{
    console.log(`Car ${carData.id} is a ${carData.car_year} ${carData.car_make} ${carData.car_model}`);
 }
