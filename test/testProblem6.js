const { inventory } = require("../dataSet");
const { getSpecificCars } = require("../problem6");

const specificData = getSpecificCars( inventory );

if( specificData == null ){
    console.log("No BMW or Audi car available");
}
else{
    console.log(JSON.stringify(specificData));
}