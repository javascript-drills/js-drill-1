const { inventory } = require("../dataSet");
const { getYears } = require("../problem4");

const yearData = getYears( inventory );

if(yearData == null){
    console.log("No data available");
}
else{
    console.log(yearData);
}