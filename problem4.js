// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getYears( inventory ){
    let year_Arr = [];
    for( let index=0; index<inventory.length; index++){
        year_Arr.push(inventory[index].car_year);
    }
    if( year_Arr.length === 0 ){
        return null;
    }
    return year_Arr;
}

module.exports = {getYears};